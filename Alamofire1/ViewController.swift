//
//  ViewController.swift
//  Alamofire1
//
//  Created by apple on 1/30/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        // Do any additional setup after loading the view.
    }

    func fetchData() {
        Alamofire.request("https://jsonplaceholder.typicode.com/todos").responseJSON{
            response in
            
            
            
            
            if let json = response.result.value{
                print("JSON: \(json)")
            
            
            
        }
        
    }
    
    
}

}
